/*
 * Hello!
 * This is a little Linux game for the Saitek x52/x52PRO, made with nirenjan's libx52 driver
 * https://github.com/nirenjan/libx52
 *
 * Find the right combination of mode, axis positions and button states to zero out the values.
 *
 * Each active axis may affect one or more columns
 * All active buttons behave as toggles
 * Each mode offers a different multiplier of axis values
 *
 * TODO: Add a licence (probably GPL)
 * TODO: Resolve issues with MFD updates hanging (probably updating too often?)
 * TODO: Resolve issues with sleep causing output lag
 * TODO: Add a menu where deadzone and dimension count can be configured
 * TODO: Add an optional timer
 * TODO: Work out how to toggle individual MFD pixels and plot output as a series of vertical linear gauges
 */


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <libx52/libx52.h>
#include <libx52/libx52io.h>

#define EMPTYLINE "                "
#define DEADZONE 20
#define DIMENSIONS 6
#define FRAMETARGET 0.0625
#define AXIS_MULTIPLIER_MINOR 0.25
#define AXIS_MULTIPLIER_DOMINANT 1.0

/* Initialise driver and joystick connection as recommended in libx52 docs */
libx52_device* init_libx52(void)
{
	libx52_device *dev;
	int rc;

	// Initialize the libx52 library
	rc = libx52_init(&dev);
	if (rc != LIBX52_SUCCESS) {
		fputs(libx52_strerror(rc), stderr);
		return NULL;
	}

	// Connect to the supported joystick
	rc = libx52_connect(dev);
	if (rc != LIBX52_SUCCESS) {
		fputs(libx52_strerror(rc), stderr);
		// A failure usually just means that there is no joystick connected
		// Look at the return codes for more information.
		return NULL;
	}

	return dev;
}

/* Print relevant error if present or return true */
bool processError(int err)
{
	if (err != LIBX52_SUCCESS)
	{
		fputs(libx52_strerror(err), stderr);
		return false;
	}
	return true;
}

/* Print relevant IO error if present or return true */
bool processIOError(int err)
{
	if (err != LIBX52IO_SUCCESS)
	{
		fputs(libx52io_strerror(err), stderr);
		return false;
	}
	return true;
}

/* Update values with current axis values multiplied by bindings */
void processAxisStates(libx52io_report* report, int checkListAxis[], unsigned int checkListAxisSize, float axisBindings[][DIMENSIONS], float modeBindings[], int values[])
{
	for (int i = 0; i < checkListAxisSize; i++)
	{
		for (int j = 0; j < DIMENSIONS; j ++)
		{
			values[j] += axisBindings[i][j] * report->axis[checkListAxis[i]] * modeBindings[report->mode - 1];
		}
	}
}

/* Update values with current toggle states */
void processToggleStates(libx52io_report* target, bool buttonStates[], int checkListButton[], unsigned int checkListButtonSize, int buttonBindings[][DIMENSIONS], int values[])
{
	for (int i = 0; i < checkListButtonSize; i++)
	{
		if (buttonStates[checkListButton[i]] == target->button[checkListButton[i]])
		{
			for (int j = 0; j < DIMENSIONS; j++)
			{
				values[j] += buttonBindings[i][j];
			}
		}
	}
}

/* Print current values, toggle states, and mode */
void updateDisplay(libx52io_report* report, int values[], int valueTargets[], int valueOffsets[], bool buttonStates[], int checkListButton[], unsigned int checkListButtonSize)
{
	printf("\n    Find the combination\n\n");
	for (int i = 0; i < DIMENSIONS; i++)
	{
		printf("%10s", (valueOffsets[i] + values[i] > DEADZONE) ? "+" : " ");
	}
	printf("\n");
	for (int i = 0; i < DIMENSIONS; i++)
	{
		printf("%9d%s", valueOffsets[i] + values[i], (abs(valueOffsets[i] + values[i]) <= DEADZONE) ? "•" : " ");
	}
	printf("\n");
	for (int i = 0; i < DIMENSIONS; i++)
	{
		printf("%10s", (valueOffsets[i] + values[i] < - DEADZONE) ? "-" : " ");
	}

	printf("\n\n");
	printf("    Mode %u", report->mode);
	for (int i = 0; i < checkListButtonSize; i++)
	{
		printf("    \tButton %d%s", i + 1, buttonStates[checkListButton[i]] ? "•" : " ");
	}
	printf("\n");

	/*
	char output[17] = EMPTYLINE;

	//TODO: Update to reflect dimensionality
	sprintf(output, "all of the above");

	//FIXME: Sometimes the update call gets stuck and the application hangs until it times out
	libx52_set_text(x52, 2, output, 16);
	processError(err);
	err = libx52_update(x52);
	if (err != LIBX52_ERROR_TIMEOUT)
	{
		run = processError(err);
	}
	*/

}

/* Check for successful combination within deadzones */
bool checkValues(int values[], int valueTargets[])
{
	for (int i = 0; i < DIMENSIONS; i++)
	{
		if (values[i] < valueTargets[i] - DEADZONE || values[i] > valueTargets[i] + DEADZONE)
		{
			return false;
		}
	}
	return true;
}

/* Updated cached button toggle state for any button that has just been pressed */
void updateToggleStates(libx52io_report* report, libx52io_report* lastReport, bool buttonStates[])
{
	for (int i = 0; i < LIBX52IO_BUTTON_MAX; i++)
	{
		if (report->button[i] && report->button[i] != lastReport->button[i])
		{
			buttonStates[i] = !buttonStates[i];
		}
	}
}

/* Set up bindings for axes and update value targets accordingly */
void setupAxisStates(libx52io_report* target, int checkListAxis[], unsigned int checkListAxisSize, float axisBindings[][DIMENSIONS], int valueTargets[])
{
	for (int i = 0; i < checkListAxisSize; i++)
	{
		//Pick a random target value for the axis
		target->axis[checkListAxis[i]] = rand() % 255;
		int count = 0;

		//Loop through all available dimensions with 50% chance to assign minor multiplier
		for (int j = 0; j < DIMENSIONS; j ++)
		{
			if (rand() % 2 > 0)
			{
				axisBindings[i][j] = AXIS_MULTIPLIER_MINOR;
				count += 1;
			}

			//Update value target accordingly
			valueTargets[j] += target->axis[checkListAxis[i]] * axisBindings[i][j];

			//Make sure we don't bind to too many dimensions
			if (count >= 4)
			{
				break;
			}
		}

		//Pick a random axis and give it a dominant multiplier, removing any previous multiplied value from target
		int j = rand() % DIMENSIONS;
		valueTargets[j] -= target->axis[checkListAxis[i]] * axisBindings[i][j];
		axisBindings[i][j] = AXIS_MULTIPLIER_DOMINANT;
		valueTargets[j] += target->axis[checkListAxis[i]] * axisBindings[i][j];
	}
}


/* Set up bindings for buttons and update value targets accordingly */
void setupButtonStates(libx52io_report* target, int checkListButton[], unsigned int checkListButtonSize, int buttonBindings[][DIMENSIONS], int valueTargets[])
{
	for (int i = 0; i < checkListButtonSize; i++)
	{
		//50% chance to be active
		target->button[checkListButton[i]] = rand() % 2;
		if (target->button[checkListButton[i]] > 0)
		{
			//Pick a random dimension, and add an amount to value targets for it
			int j = rand() % DIMENSIONS;
			buttonBindings[i][j] = (rand() % 10 + 10) * 50;
			if (rand() % 2)
			{
				//50% chance to be negative
				buttonBindings[i][j] *= -1;
			}
			valueTargets[j] += buttonBindings[i][j];
		}
	}
}

/* Set value offsets to zero out against targets */
void setupValueOffsets(int valueTargets[], int valueOffsets[])
{
	for (int i = 0; i < DIMENSIONS; i++)
	{
		valueOffsets[i] = -valueTargets[i];
	}
}

/* Initialise and run game */
int run(libx52_device* x52)
{
	bool run = true;
	double frameTime = 0.0;
	clock_t frameStart;
	int err;

	libx52io_context* context;
	libx52io_report report;
	libx52io_report lastReport;
	libx52io_report target;

	int checkListButton[] = {
						LIBX52IO_BTN_TRIGGER,
						LIBX52IO_BTN_PINKY,
						LIBX52IO_BTN_A,
						LIBX52IO_BTN_B,
/*						LIBX52IO_BTN_C,
						LIBX52IO_BTN_D,
						LIBX52IO_BTN_E,
/*						LIBX52IO_BTN_T1_UP,
						LIBX52IO_BTN_T2_UP,
						LIBX52IO_BTN_T3_UP,
						LIBX52IO_BTN_T1_DN,
						LIBX52IO_BTN_T2_DN,
						LIBX52IO_BTN_T3_DN,
						LIBX52IO_BTN_POV_1_N,
						LIBX52IO_BTN_POV_1_S,
						LIBX52IO_BTN_POV_1_E,
						LIBX52IO_BTN_POV_1_W,
						LIBX52IO_BTN_POV_2_N,
						LIBX52IO_BTN_POV_2_S,
						LIBX52IO_BTN_POV_2_E,
						LIBX52IO_BTN_POV_2_W,*/
					};
	int checkListButtonSize = sizeof(checkListButton) / sizeof(checkListButton[0]);
	int checkListAxis[] = {
						LIBX52IO_AXIS_Z,
						LIBX52IO_AXIS_RX,
						LIBX52IO_AXIS_RY,
						LIBX52IO_AXIS_SLIDER,
					};
	int checkListAxisSize = sizeof(checkListAxis) / sizeof(checkListAxis[0]);


	int valueTargets[DIMENSIONS] = {[0 ... DIMENSIONS -1] = 0};
	int valueOffsets[DIMENSIONS] = {[0 ... DIMENSIONS -1] = 0};

	bool buttonStates[LIBX52IO_BUTTON_MAX] = {[0 ... LIBX52IO_BUTTON_MAX - 1] = false};

	float modeBindings[3] = {1.0f, 0.5f, -0.5f};

	float axisBindings[checkListAxisSize][DIMENSIONS];
	for (int i = 0; i < checkListAxisSize; i ++)
	{
		for (int j = 0; j < DIMENSIONS; j++)
		{
			axisBindings[i][j] = 0.0f;
		}
	}

	int buttonBindings[checkListButtonSize][DIMENSIONS];
	for (int i = 0; i < checkListButtonSize; i ++)
	{
		for (int j = 0; j < DIMENSIONS; j++)
		{
			buttonBindings[i][j] = 0;
		}
	}

	printf("Init\n");
	err = libx52io_init(&context);
	run = processIOError(err);
	if (run)
	{
		printf("Open\n");
		err = libx52io_open(context);
		run = processIOError(err);
	}

	//Clear MFD
	libx52_set_text(x52, 0, EMPTYLINE, 16);
	libx52_set_text(x52, 1, EMPTYLINE, 16);
	libx52_set_text(x52, 2, EMPTYLINE, 16);
	libx52_update(x52);

	//Read current values to initialise target
	err = libx52io_read(context, &target);
	run = processIOError(err);

	//Read current values to use as initial state
	err = libx52io_read(context, &lastReport);
	run = processIOError(err);

	srand(time(NULL));

	//Set up target report and dimension value targets
	setupButtonStates(&target, checkListButton, checkListButtonSize, buttonBindings, valueTargets);
	setupAxisStates(&target, checkListAxis, checkListAxisSize, axisBindings, valueTargets);
	setupValueOffsets(valueTargets, valueOffsets);

	//Raondimise mode behaviours
	for (int i = 0; i < 3; i++)
	{
		int j = rand() % 3;
		float temp = modeBindings[i];
		modeBindings[i] = modeBindings[j];
		modeBindings[j] = temp;
	}

	//Game loop
	while (run)
	{
		frameStart = clock();
		err = libx52io_read_timeout(context, &report, 1);
		if (err == LIBX52IO_ERROR_TIMEOUT)
		{
			continue;
		}
		run = processIOError(err);
		if (run)
		{
			if (report.button[LIBX52IO_BTN_FIRE] != 0)
			{
				printf("\nQuitting\n");
				run = false;
			}
			else
			{
				//Clear display
				printf("\e[1;1H\e[2J");

				//Update toggle states with any new button presses
				updateToggleStates(&report, &lastReport, buttonStates);

				int values[DIMENSIONS] = {[0 ... DIMENSIONS -1] = 0};
				processAxisStates(&report, checkListAxis, checkListAxisSize, axisBindings, modeBindings, values);
				processToggleStates(&target, buttonStates, checkListButton, checkListButtonSize, buttonBindings, values);

				updateDisplay(&report, values, valueTargets, valueOffsets, buttonStates, checkListButton, checkListButtonSize);

				lastReport = report;
				if (checkValues(values, valueTargets))
				//if (matches == DIMENSIONS)
				{
					printf("\nSuccess!\n");
					run = false;
				}
				else
				{
					fflush(stdout);
					frameTime = (double)(clock() - frameStart) / CLOCKS_PER_SEC;
					if (frameTime < FRAMETARGET)
					{
						//FIXME: This behaves inconsistently
						//printf("Sleeping %f\n", (FRAMETARGET - frameTime) * 1000000);
						//usleep((FRAMETARGET - frameTime) * 1000000);
					}
				}
			}
		}
		else
		{
			printf("Error while reading?");
		}
	}

	libx52_set_text(x52, 0, EMPTYLINE, 16);
	libx52_set_text(x52, 1, EMPTYLINE, 16);
	libx52_set_text(x52, 2, EMPTYLINE, 16);
	libx52_update(x52);
}

int main(int argc, char **argv)
{
	libx52_device* x52 = init_libx52();

	if (x52 == NULL)
	{
		printf("Can't initialise x52");
		exit(1);
	}
	else
	{
		run(x52);
	}

	// Close the library and any associated devices
	libx52_exit(x52);
}
